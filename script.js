'use strict';
let user={
    id:1,
    name:"Liverpool",
    login:"LFC_YNWA",
    password:"12345qwerty",
    isAuthorized:false
}
console.log(user)

let post={
    id:1,
    user_id:user.id,
    title:"Season_19/20",
    dateTime:"02.04.2020",
    author:user.name,
    like:false
}
console.log(post)

let comment={
    id:1,
    post_id:post.id,
    name:user.name,
    text:"Fresh post",
    like:1
}
console.log(comment)

let posts=["post_1....", "post_2...."]
function addPost(post){
    posts.push(post)
}
addPost(post),
addPost("post_4....")
console.log(posts)

function changeAuthorizationState(user) {
    return user.isAuthorized=true;
}
console.log(user)
changeAuthorizationState(user)
console.log(user)

function updateLike(r){
    for(let i = 0; i<posts.length; i++){
        if(posts[i] === r){
            if(posts[i].like==false){
                return posts[i].like=true;
            }else{
                return posts[i].like=false;
            }
        }
    }
}

updateLike(post)
console.log(posts)
